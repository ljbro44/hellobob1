import unittest, requests
class TestApp(unittest.TestCase):
    def test_add(self):
        response = requests.get('http://13.209.15.210:8136//add?a=1&b=1')
        self.assertEqual(response.text, '2')
    def test_sub(self):
        response = requests.get('http://13.209.15.210:8136//sub?a=3&b=5')
        self.assertEqual(response.text, '-2')
if __name__ == "__main__":
    unittest.main()