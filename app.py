from flask import Flask, request

class App:
    app = Flask(__name__)
    
    @app.route('/')
    def hello_world():
        return 'Hello bob from 이주형'

    @app.route('/add')
    def add():
        a = (int)(request.args.get('a'))
        b = (int)(request.args.get('b'))
        return str(a+b)

    @app.route('/sub')
    def sub():
        a = (int)(request.args.get('a'))
        b = (int)(request.args.get('b'))
        return str(a-b)

if __name__ == '__main__':
    a = App()
    a.app.run(host='0.0.0.0', port=8136)